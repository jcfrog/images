Diaporama web d'initiation aux images numériques, essentiellement matricielles, les images vectorielles sont évoquées succinctement. L'idée est de faire un survol assez général des notions et outils de bases. Idéalement à présenter avec un logiciel en parallèle pour quelques démos pratiques. Il s'agit plus de donner une idée de ce qu'on peut faire et avec quels outils qu'un apprentissage de Gimp par exemple. Ce sont des notions essentielles que nous avons présentées avant d'attaquer des atelier plus pratiques.

Notions : Pixels et dimensions, résolutions, couleurs, transparence, calques, masques, gradients, formats ...

Outils : Sélectionner, recadrer, redimensionner, retoucher, filtres, stickers, ...

Fonctionnement : se joue dans un navigateur, utilise Reveals.js, les slides sont animés et s’enchaînent horizontalement et verticalement, appuyer sur la barre d'espace pour avancer, shift+espace pour reculer. Il y a une 50aine de diapo, présentée une fois en 1h30.

La présentation est très graphique, peu de texte. Elle contient notamment un répertoire "assets" pour toutes les illustrations et un répertoire "photos" contenant des images libres pour d'éventuelles exercices. Le répertoire "workshop" contient quelques fichiers sources d'illustration au format PNG Fireworks.

Niveau : débutant

Note : quelques GIFs animés sont utilisés mais ne semblent pas toujours se jouer. Dans ce cas j'utilise un click droit + "Visualiser l'image" pour avoir l'animation, puis retour arrière pour revenir à la page de la présentation.

Démo disponible en ligne : http://adn56.net/pres/images